<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;
use App\Region;
use App\Country;
use App\State;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Country::truncate();
        $this->call(CountriesTableSeeder::class);

        Region::truncate();
        $this->call(RegionsTableSeeder::class);

    	State::truncate();
        $this->call(StatesTableSeeder::class);
    }
}
