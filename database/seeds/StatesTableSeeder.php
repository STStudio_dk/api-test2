<?php

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\State;

class StatesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('en_US');

		foreach (range(1, 30) as $index) {
			State::create([
				'country_id' => $faker->numberBetween($min = 1, $max = 10),
				'name' => $faker->state()
				// 'polygon' => 

			]);
		}
    }
}
