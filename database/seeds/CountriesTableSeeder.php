<?php

use Faker\Factory as Faker;
// use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Country;

/**
* 
*/
class CountriesTableSeeder extends Seeder
{
	
	public function run() {
		$faker = Faker::create('da_DK');

		foreach (range(1, 10) as $index) {
			Country::create([
				'name' => $faker->country(),
				'code' => $faker->locale()
			]);
		}
	}
}