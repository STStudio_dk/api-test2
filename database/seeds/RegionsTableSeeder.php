<?php

use Faker\Factory as Faker;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;
use App\Region;

/**
* 
*/
class RegionsTableSeeder extends Seeder
{
	
	public function run() {
		$faker = Faker::create('da_DK');

		foreach (range(1, 10) as $index) {
			Region::create([
				'country_id' => $faker->numberBetween($min = 1, $max = 10),
				'name' => $faker->region()
				// 'polygon' => 

			]);
		}
	}
}